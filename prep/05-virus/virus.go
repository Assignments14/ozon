package main

import (
    "fmt"
    "bufio"
    "os"
    "strconv"
    "encoding/json"
)

func check(e error) {
    if e != nil {
        panic(e)
    }
}

func main() {

    var in *bufio.Reader
    var out *bufio.Writer

    in = bufio.NewReader(os.Stdin)
    out = bufio.NewWriter(os.Stdout)
    defer out.Flush()

    var (
        err error
        t, n int
    )

    scanner := bufio.NewScanner(in)
    scanner.Scan()
    t, err = strconv.Atoi(scanner.Text())
    check(err)
    //fmt.Fprintln(out, t)

    for i := 0; i < t; i++ {
        scanner.Scan()
        n, err = strconv.Atoi(scanner.Text())
        check(err)
        //fmt.Fprintln(out, n)

        var js string
        for j := 0; j < n; j++ {
            scanner.Scan()
            js += scanner.Text()
        }

        var ls map[string]interface{}
        json.Unmarshal([]byte(js), &ls)
        fmt.Fprintln(out, ls["folders"])

    }


}
