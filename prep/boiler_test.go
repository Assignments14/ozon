package vlc

import (
	"reflect"
	"testing"
)

func Test_main(t *testing.T) {

	type args struct {
		a	int
		b	int
	}

	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "1 set",
			args: args{a: 256, b: 42},
			want: 298,
		}, 
		{
			name: "2 set",
			args: args{a: 1000, b: 1000},
			want: 2000,
		}, 
	}
	for _, tt := range tests {
        t.Error(tt.args.a)
        /*
		t.Run(tt.name, func(t *testing.T) {
			if got := splitByChunks(tt.args.bStr, tt.args.chunkSize); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("splitByChunks() = %v, want %v", got, tt.want)
			}
		})
        */
	}
}

