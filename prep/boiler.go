package main

import (
    "fmt"
    "bufio"
    "os"
)

func main() {

    var in *bufio.Reader
    var out *bufio.Writer

    in = bufio.NewReader(os.Stdin)
    out = bufio.NewWriter(os.Stdout)
    defer out.Flush()

    var n, a, b int

    fmt.Fscan(in, &n)

    for i := 0; i < n; i++ {
        fmt.Fscan(in, &a, &b)
        fmt.Fprint(out, a + b)
    }

}
