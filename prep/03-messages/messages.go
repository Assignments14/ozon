package main

import (
    "fmt"
    "bufio"
    "os"
)

func main() {

    var in *bufio.Reader
    var out *bufio.Writer

    in = bufio.NewReader(os.Stdin)
    out = bufio.NewWriter(os.Stdout)
    defer out.Flush()

    var n, q, t, u, messageId int

    fmt.Fscan(in, &n, &q)

    var comms map[int]int
    comms = make(map[int]int, n)

    messageId = 0

    for i := 0; i < q; i++ {
        fmt.Fscan(in, &t, &u)

        if t == 1 {
            messageId++
            comms[u] = messageId
            continue
        }
    
        lastId := comms[u]
        if comms[0] > lastId {
            lastId = comms[0]
        }

        fmt.Fprintln(out, lastId)
    }

}
