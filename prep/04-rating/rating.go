package main

import (
    "fmt"
    "bufio"
    "os"
    "sort"
)

func main() {

    var in *bufio.Reader
    var out *bufio.Writer

    in = bufio.NewReader(os.Stdin)
    out = bufio.NewWriter(os.Stdout)
    defer out.Flush()

    var t, n, lapse int
    fmt.Fscan(in, &t)

    for i := 0; i < t; i++ {
        fmt.Fscan(in, &n)

        times := make([]int, n)
        for j := 0; j < n; j++ {
            fmt.Fscan(in, &lapse)
            times[j] = lapse
        }

        timesSorted := make([]int, n)
        copy(timesSorted, times)
        sort.Ints(timesSorted)
        //fmt.Fprintf(out, "%v\n", timesSorted)

        ratings := make(map[int]int, n)
        
        currRating := 1
        ratings[timesSorted[0]] = currRating

        for j := 1; j < n; j++ {
            prevTime := timesSorted[j - 1]
            currTime := timesSorted[j]
            if (currTime - prevTime) > 1 {
                currRating = j + 1
            }
            ratings[currTime] = currRating
        }

        for j := 0; j < n; j++ {
            fmt.Fprint(out, ratings[times[j]], " ")
        }

        //fmt.Fprintln(out, ratings)
        fmt.Fprint(out, "\n")

        //fmt.Printf("%v\n", times)
        //fmt.Printf("%v\n", timesSorted)
        //fmt.Println(ratings)
        
    }

}
