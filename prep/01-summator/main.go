/*
Прежде чем вы приступите к решению задач, используйте эту, чтобы познакомиться с платформой Techpoint, а также получить первые 5 баллов.
Напишите программу, которая выводит сумму двух целых чисел.

>>
5
256 42
1000 1000
-1000 1000
-1000 1000
20 22

<<
298
2000
0
0
42

*/

package main

import (
    "fmt"
    "bufio"
    "os"
)

func main() {

    var in *bufio.Reader
    var out *bufio.Writer

    in = bufio.NewReader(os.Stdin)
    out = bufio.NewWriter(os.Stdout)
    defer out.Flush()

    var n, a, b int

    fmt.Fscan(in, &n)

    for i := 0; i < n; i++ {
        fmt.Fscan(in, &a, &b)
        fmt.Fprint(out, a + b, "\n")
    }
    
}

