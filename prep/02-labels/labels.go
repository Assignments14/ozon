package main

import (
    "fmt"
    "bufio"
    "os"
)

func main() {

    var in *bufio.Reader
    var out *bufio.Writer

    in = bufio.NewReader(os.Stdin)
    out = bufio.NewWriter(os.Stdout)
    defer out.Flush()

    var s, r string
    fmt.Fscan(in, &s)

    sr := []rune(s)

    var n, a, b int
    fmt.Fscan(in, &n)

    for i := 0; i < n; i++ {
        fmt.Fscan(in, &a, &b, &r)
        for k, x := a, 0; k <= b; k, x = k+1, x+1 {
            sr[k-1] = rune(r[x])
        }
    }
    
    //fmt.Fprint(out, s, "\n")
    fmt.Fprint(out, string(sr), "\n")

}
